import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

df= pd.read_csv('./Data/MCGM_DATA.csv', header=0)
train, test = train_test_split(df, test_size=0.1)

train.to_csv('./Data/MCGM_DATA_train.csv', sep=',', index = False)
test.to_csv('./Data/MCGM_DATA_test.csv', sep=',', index = False)
print("Congrats your files have been created !!!")