"""
Author = Meet Shah(a.k.a slim_shah)

Date:	 11/05/2018.

Version: 1.0.

Title:   Randomly Sampling Data from csv file

Purpose: This script accepts two argument. 1st is filename and 2nd is 'frac' which signifies the fraction of file you want to sample
		 After taking frac it will break file into two parts 1) percent.csv 2) rest.csv 
		 keeping the original file.

		 Note: please make sure file is present in the same directory as script.
		 	   also make sure frac is in fraction and not actual percentage.
		 	   df_percent = DF.sample(frac = 0.7)

Source:  https://stackoverflow.com/questions/15923826/random-row-selection-in-pandas-dataframe	 
"""

import pandas as pd
import sys
filename = sys.argv[1]
#print(filename)
Filename = filename.replace('.csv', '')
Frac = float(sys.argv[2])
DF = pd.read_csv(filename, header = 0)
# Randomly sample 50% of your dataframe
df_percent = DF.sample(frac = Frac)

# Randomly sample 7 elements from your dataframe
#df_elements = df.sample(n=7)

# For getting rest of file
df_rest = DF.loc[~DF.index.isin(df_percent.index)]

df_percent.to_csv(Filename + '_train.csv', sep=',', index = False)
df_rest.to_csv(Filename  + '_test.csv', sep=',', index = False)
print("Congrats your files have been created !!!")