"""
Author = Meet Shah(a.k.a slim_shah)
Date:	 25/05/2018.
Version: 1.0.
Title:   Extracting Potential-Marathi Stop words.
Purpose: I am writing this code to make set potential marathi stop words which will be later used to pre-process MCGM DATA.
source:  Google Translate => https://pypi.org/project/googletrans/
         Alphabet-Matching Regex => https://stackoverflow.com/questions/89909/how-do-i-verify-that-a-string-only-contains-letters-numbers-underscores-and-da
"""

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from googletrans import Translator
import _pickle as pickle
import os
import pandas as pd
import re
import random
import pandas as pd
import time

class MARATHI_STOP_WORDS:
    MARATHI_WORDS = set()
    MARATHI_STOP_WORDS = set()
    def __init__(self):
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))

    def isEnglish(self, s):
        try:
            s.encode(encoding='utf-8').decode('ascii')
        except UnicodeDecodeError:
            return False
        else:
            return True

    def Read_file(self):
        PATH = os.path.join(self.BASE_DIR, "MCGM_DATA.csv")
        DF = pd.read_csv(PATH, header = 0, encoding = 'utf-8', usecols=['Problem Description'])
        DF.dropna(axis=0, inplace=True)
        for index, row in DF.iterrows():
            try:
                #WORDS = word_tokenize(row['Problem Description'])
                WORDS = row['Problem Description'].split(' ')
                for each_word in WORDS:
                    if self.isEnglish(each_word):
                        continue
                    else:
                        if '/' not in each_word:
                            self.MARATHI_WORDS.add(each_word)
            except Exception as e:
                print(row['Problem Description'])
                print(e)
        print('length of set is: ',len(self.MARATHI_WORDS))
        
        # Save Marathi Words that are extracted from Mgcm file.        
        save_marathi_words = open("Marathi_Words.pickle", "wb")
        pickle.dump(self.MARATHI_WORDS, save_marathi_words)
        save_marathi_words.close()
    
    def Create_Stop_Words(self):
        print('Processing')
        translator = Translator()
        stop_words = set(stopwords.words("english"))
        MARATHI_STOP_WORDS = set()
        i =0
        total_len = len(stop_words)
        dictionary = {}

        for each_word in stop_words:
            temp = translator.translate(each_word, dest='mr')
            Translation = temp.text.lower()

            MARATHI_STOP_WORDS.add(Translation)
            dictionary[Translation] = each_word
            i+=1
            per = int((i/total_len)*100)
            if per%10 == 0:
                print('Percentage complete: ', per)

        # Save Marathi stop words
        marathistop_words_pickle = open("Marathi_Stop_Words.pickle", "wb")
        pickle.dump(MARATHI_STOP_WORDS, marathistop_words_pickle)
        marathistop_words_pickle.close()

        # Save dictionary
        dict_pickle = open("dict.pickle", "wb")
        pickle.dump(dictionary, dict_pickle)
        dict_pickle.close()


    def Save_In_Text_File(self):
        dict_pickle = open("dict.pickle", "rb")
        _Dictionary = pickle.load(dict_pickle)
        dict_pickle.close()
        with open("dict.txt","w") as f:
            for i in _Dictionary:
                f.write(str(i) + ' ' + str(_Dictionary[i]) + "\n")

    def save_set_into_file(self):
        # Opening list of remaining words
        remaining_pickle = open("remaining.pickle", "rb")
        remaining = pickle.load(remaining_pickle)
        remaining_pickle.close()

        print('Length of remaining words: ', len(remaining))
        df = pd.DataFrame({'Category': remaining})

        df.to_csv('marathi.csv', index = False, header = False)


Temp = MARATHI_STOP_WORDS()
#Temp.save_set_into_file()
#Temp.Read_file()
Temp.Create_Stop_Words()
Temp.Save_In_Text_File()
# Temp.save_set_into_file()