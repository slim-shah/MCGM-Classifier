"""
Author = Meet Shah(a.k.a slim_shah)
Date:	 31/05/2018.
Version: 1.0.
Title:   Sub-Category Classification.
Purpose: I am writing this code to classify mcgm sub-category from problem description.
"""
import numpy as np
import pandas as pd
from sklearn.externals import joblib
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer, CountVectorizer
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import random
import _pickle as pickle
import os.path

class MCGM_Naive_Bayes:
    MARATHI_STOP_WORDS = ''
    MARATHI_CLASSIFIER = ''
    ENGLISH_CLASSIFIER = ''

    def __init__(self):
        #Base Directory
        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))

        # Open marathi stop words
        MARATHI_STOP_WORDS_PICKLE = open("Marathi_Stop_Words.pickle", "rb")
        self.MARATHI_STOP_WORDS = pickle.load(MARATHI_STOP_WORDS_PICKLE)
        MARATHI_STOP_WORDS_PICKLE.close()

        # Create Stop Words List
        self.STOP_WORDS = self.MARATHI_STOP_WORDS.union(stopwords.words("english"))

        if os.path.isfile(os.path.join(self.BASE_DIR, "Random_Forest.pickle")) and os.path.isfile(os.path.join(self.BASE_DIR, "vectorizer.sav")):
            print("Loaded from file")
            # Open Random Forest Classifier
            Random_Forest_Pickle = open("Random_Forest.pickle", "rb")
            self.RANDOM_CLASSIFIER = pickle.load(Random_Forest_Pickle)
            Random_Forest_Pickle.close()

            '''
            # Open CountVector
            vecotrizer_Pickle = open("Count_Vector.pickle", "rb")
            self.vectorizer = pickle.load(vecotrizer_Pickle)
            vecotrizer_Pickle.close()
            '''
            self.vectorizer = joblib.load("vectorizer.sav")
            #self.RANDOM_CLASSIFIER = joblib.load("Random_Forest_Classifier.sav")

        else:
            self.RANDOM_CLASSIFIER = RandomForestClassifier(n_estimators=100)
            self.vectorizer = CountVectorizer(tokenizer=None,
                                              stop_words=None,
                                              preprocessor=None,
                                              max_features=5000)
        print("Initialization complete")

    def isEnglish(self, s):
        try:
            s.encode(encoding='utf-8').decode('ascii')
        except UnicodeDecodeError:
            return False
        else:
            return True

    def chunker(self, seq, size):
        return (seq[pos:pos + size] for pos in range(0, len(seq), size))

    def clean(self, COMPLAINT):
        words = word_tokenize(COMPLAINT)

        for each in words:
            if each in self.STOP_WORDS:
                words.remove(each)

            # if any(map( lambda c:c.isdigit(), each)):
            #     print(each)
            #     words.remove(each)

            if each.isdigit():
                #print(each)
                words.remove(each)

        COMPLAINT = ' '.join(words)
        return COMPLAINT

    def TRAIN(self, Train_DF):
        j = 1
        print("Training the random forest")
        for i in self.chunker(Train_DF, 10000):
            X = []
            Y = []
            for index, row in i.iterrows():
                X.append(self.clean(row["Problem Description"]))
                Y.append(row['Sub-Category'])

            X_train = self.vectorizer.fit_transform(X)
            X_train = X_train.toarray()

            #print(X_train.shape)
            #vocab = self.vectorizer.get_feature_names()
            #print('length of vocab',len(vocab))
            '''
            #sum up and print the counts of each vocabulary words
            dist = np.sum(X_train, axis =0)
    
            for tag,count in zip(vocab, dist):
                 print(count, tag)
            '''
            self.RANDOM_CLASSIFIER = self.RANDOM_CLASSIFIER.fit(X_train, Y)

            print('Chunk: ', j)
            j+=1
            #End of For Loop

        # Save Random Forest Classifier.
        Random_Forest_Pickle = open("Random_Forest.pickle", "wb")
        pickle.dump(self.RANDOM_CLASSIFIER, Random_Forest_Pickle)
        Random_Forest_Pickle.close()

        # Save count Vector
        Count_Vector_Pickle = open("Count_Vector.pickle", "wb")
        pickle.dump(self.vectorizer, Count_Vector_Pickle)
        Count_Vector_Pickle.close()

        joblib.dump(self.RANDOM_CLASSIFIER, 'Random_Forest_Classifier.sav')
        joblib.dump(self.vectorizer, 'vectorizer.sav')

    def TEST(self, COMPLAINT):
        Clean_input = self.clean(COMPLAINT)

        #vocab = self.vectorizer.get_feature_names()
        #print(vocab)
        #print(Clean_input)
        X_train = self.vectorizer.transform([Clean_input])
        return self.RANDOM_CLASSIFIER.predict(X_train.toarray())


def main():
    print("Program started")
    TRAIN = pd.read_csv('./Data/MCGM_DATA_train.csv', header=0, encoding='utf-8', usecols=['Sub-Category', 'Problem Description'])
    TRAIN.dropna(axis=0, inplace=True)

    TEST = pd.read_csv('./Data/MCGM_DATA_test.csv', header=0, encoding='utf-8', usecols=['Sub-Category', 'Problem Description'])
    TEST.dropna(axis=0, inplace=True)

    Classifier = MCGM_Naive_Bayes()

    Classifier.TRAIN(TRAIN)
    print('Training completed')

    X = TEST['Problem Description'].tolist()
    Y = TEST['Sub-Category'].tolist()

    accuracy = 0
    RESULT = []
    ORIGINAL = []
    for each in range(0, len(X)):
        result = Classifier.TEST(X[each])
        RESULT.append(result[0])
        ORIGINAL.append(Y[each])
        if result[0] == Y[each]:
            accuracy+=1
    print("Accuracy: ", (accuracy/len(X))*100,'%' )

    df = pd.DataFrame(data={"Prediction": RESULT, "Original": ORIGINAL})
    df.to_csv("result.csv", index=False, sep=',')


main()